const Webhook = {
    FITNANCE_READ: "fitnance_read",
    EVENT_CREDENTIAL_TOKEN_CREATION: "event_credential_token_creation"
}

export {
    Webhook
};

export default {
    Webhook
};
