import axios from 'axios';

function Tokenized(token, client=axios) {
    return {
      init: async (data) => {
        const headers = token.headers,
            method = "post",
            url = `${token.baseUrl}/api/v3/reader/tokenized/`;

        const response = await client({method, url, data, headers});
        if(response.status !== 202) {
            throw new Error(JSON.stringify(response));
        }
        return response.data.response;
      }
    }
}

export {
    Tokenized
};
