# Unnax SDK

## Getting Started

### Installing
npm:

```sh
$ npm install unnax
```

### Examples
#### Tokenized credentials widget
```js
import { Token, WidgetTokenizeCredentials } from 'unnax';

const token = Token("API_ID", "API_CODE", "https://demo.unnax.com"),
    tokenizeCredentials = TokenizeCredentials(token);

tokenizeCredentials.init({request_code: "1234"}).then((response) => console.log(response));
```

#### Reader Tokenized
```js
import { Token, ReaderTokenized } from 'unnax';

const token = Token("API_ID", "API_CODE", "https://demo.unnax.com"),
    tokenizedRead = ReaderTokenized(token);

const requestData = {
    token_key: "TOKEN",
    token_id: "ID",
    request_code: "REQ_CODE",
    start_date : "2016-01-01"
}
tokenizedRead.init(requestData);
```

#### Webhooks
```js
import { Token, Webhook, Codes } from 'unnax';

const fitnanceWebhook = {
    client: "callback",
    event: "fitnance_read",
    target: "https://www.your_web_url.com/endpoint"
};
const tokenCreationWebhook = {
    event: Codes.Webhook.EVENT_CREDENTIAL_TOKEN_CREATION,
    target: "https://www.your_web_url.com/endpoint"
};

const token = Token("API_ID", "API_CODE", "https://demo.unnax.com"),
    webhookClient = Webhook(token);
webhookClient.create(fitnanceWebhook);
webhookClient.create(tokenCreationWebhook);
```

#### Callbacks
```js
import { Token, Callback } from 'unnax';

const encryptedCallback = {
    "triggered_event": "fitnance_read",
    "data": "f6FTLnO3icis09IgV76BtCJPPNWQlkQCjmW91YF8EnBVy2PmpWSEirJm4Lk7LgTf5yqTAUsvJ0F9zensH8Xz3r8LXumKaW75MG0G+p6c554J+zyMhdI9u7TaS5rT6sPWaAOmxvKbBqpM4sGDKQqaxTOIRTOpKigPfjZJKnLADjU="
};

const token = Token("API_ID", "API_CODE", "https://demo.unnax.com"),
    decryptedCallback = Callback(encryptedCallback, token).json();
```

## Api documentation

Unnax api documentation can be found [here](https://sandbox.unnax.com/3.0/welcome/unnax-docs)
