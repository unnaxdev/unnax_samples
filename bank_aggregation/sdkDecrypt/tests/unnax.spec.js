import {expect} from 'chai';
import { decryptFitnance } from 'unnax/utils';
import { Token, Callback } from 'unnax';
import { TokenizeCredentials } from 'unnax/widget';
import { Tokenized } from 'unnax/api/reader';
import { Webhook } from 'unnax/api/webhook';


describe('unnax', () => {
    describe('widget TokenizeCredentials', () => {
        it('should get widget url', async () => {
            const fakeClient = (options) => {
                if(options.url === "/api/v3/tokenized_credentials/init/" && options.method === "post") {
                  const endpointResponse = {
                    widget_url: "WIDGET_URL"
                  };
                  return Promise.resolve({status: 200, data: endpointResponse});
                }
                return Promise.reject("Invalid url");
            };
            const token = Token("API_ID", "API_CODE", ""),
                tokenizeCredentials = TokenizeCredentials(token, fakeClient);

            const response = await tokenizeCredentials.init({request_code: "1234"});
            expect(response).to.eql({widget_url: 'WIDGET_URL'});
        });

        it('should throw error on server error', async () => {
            const fakeClient = (options) => {
                if(options.url === "/api/v3/tokenized_credentials/init/" && options.method === "post") {
                  return Promise.resolve({status: 400, data: {response: "Fail response"}});
                }
            };
            const token = Token("API_ID", "API_CODE", ""),
                tokenizeCredentials = TokenizeCredentials(token, fakeClient);

            let errorFound = false;
            try {
                const response = await tokenizeCredentials.init({request_code: "1234"});
            }
            catch (err) {
                errorFound = true;
            }
            expect(errorFound).to.be.true;
        });
    });

    describe('api', () => {
        describe('reader Tokenized', () => {
            it('should call tokenized api', async () => {
                const fakeClient = (options) => {
                    if(options.url === "/api/v3/reader/tokenized/" && options.method === "post") {
                      const endpointResponse = {
                          job_id: "JOB_ID"
                      };
                      return Promise.resolve({status: 202, data: {response: endpointResponse}});
                    }
                    return Promise.reject("Invalid url");
                };
                const token = Token("API_ID", "API_CODE", ""),
                    tokenizedRead = Tokenized(token, fakeClient);

                const requestData = {
                    token_key: "TOKEN",
                    token_id: "ID",
                    request_code: "REQ_CODE",
                    start_date : "2016-01-01"
                }
                const response = await tokenizedRead.init(requestData);
                expect(response).to.include.all.keys('job_id');
            });
        });

        describe('webhook', () => {
            it('should call webhook create api', async () => {
                const requestData = {
                    client: "callback",
                    event: "fitnance_read",
                    target: "https://www.your_web_url.com/endpoint"
                };

                const fakeClient = (options) => {
                    if(options.url === "/api/v3/webhooks/" && options.method === "post") {
                      const endpointResponse = Object.assign({}, options.data, {
                            id: 1,
                            state: 1,
                            created_at: "2017-04-11T07:46:00Z",
                            updated_at: "2017-04-11T07:46:00Z"
                      });
                      return Promise.resolve({status: 201, data: {response: endpointResponse}});
                    }
                    return Promise.reject("Invalid url");
                };

                const token = Token("API_ID", "API_CODE", ""),
                    webhookClient = Webhook(token, fakeClient);
                const response = await webhookClient.create(requestData);
                expect(response).to.include.all.keys('id', 'client', 'event', 'target');
            });

            it('should use callback as client if not defined', async () => {
                const requestData = {
                    event: "event_credential_token_creation",
                    target: "https://www.your_web_url.com/endpoint"
                };

                const fakeClient = (options) => {
                    if(options.url === "/api/v3/webhooks/" && options.method === "post") {
                      const endpointResponse = Object.assign({}, options.data, {
                            id: 1,
                            state: 1,
                            created_at: "2017-04-11T07:46:00Z",
                            updated_at: "2017-04-11T07:46:00Z"
                      });
                      return Promise.resolve({status: 201, data: {response: endpointResponse}});
                    }
                    return Promise.reject("Invalid url");
                };

                const token = Token("API_ID", "API_CODE", ""),
                    webhookClient = Webhook(token, fakeClient);
                const response = await webhookClient.create(requestData);
                expect(response.client).to.eql('callback');
            });

        });
    });

    describe('utils decryptFitnance', () => {
        it('should decrypt a fitnance response', () => {
            const encryptedData = "f6FTLnO3icis09IgV76BtCJPPNWQlkQCjmW91YF8EnBVy2PmpWSEirJm4Lk7LgTf5yqTAUsvJ0F9zensH8Xz3r8LXumKaW75MG0G+p6c554J+zyMhdI9u7TaS5rT6sPWaAOmxvKbBqpM4sGDKQqaxTOIRTOpKigPfjZJKnLADjU=",
                response = decryptFitnance(encryptedData, "API_CODE", "API_ID"),
                expected_response = {"accounts": [{"id": 1, "name": "Account1", "balance": 123}], "statements": [], "customers": []};
            expect(response).to.eql(expected_response);
        });
    });

    describe('Callback', () => {
        it('should return fitnance data', () => {
            const callbackRaw = {
                "triggered_event": "fitnance_read",
                "data": "f6FTLnO3icis09IgV76BtCJPPNWQlkQCjmW91YF8EnBVy2PmpWSEirJm4Lk7LgTf5yqTAUsvJ0F9zensH8Xz3r8LXumKaW75MG0G+p6c554J+zyMhdI9u7TaS5rT6sPWaAOmxvKbBqpM4sGDKQqaxTOIRTOpKigPfjZJKnLADjU="
            };
            const token = Token("API_ID", "API_CODE"),
                callback = Callback(callbackRaw, token);

            expect(callback.json()).to.be.an('object').that.include.all.keys('accounts', 'statements', 'customers');
        });
        it('should return token creation data', () => {
            const callbackRaw = {
                "triggered_event": "event_credential_token_creation",
                "data": {
                    "bank_id": 16,
                    "request_code": "tokenized28820171227",
                    "token_id": 43,
                    "token_key": "2yH3HMJ0SLjpezC1B6EVE4WqDbnzRfvb"
                }
            }
            const token = Token("API_ID", "API_CODE"),
                callback = Callback(callbackRaw, token);
            expect(callback.json()).to.eql(callbackRaw['data']);
        });
    });
});
