<?php

function decrypt_data($data, $key, $iv) {
    $key_size = 16;
    $iv_size = 16;
    $padding = '$';

    $cypher = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', MCRYPT_MODE_CBC, '');
    $key = substr(str_pad($key, $key_size, $padding), 0, $key_size);
    $iv = substr(str_pad($iv, $iv_size, $padding), 0, $iv_size);

    // initialize encryption handle
    if (mcrypt_generic_init($cypher, $key, $iv) != -1) {
            // decrypt
            $decrypted = mdecrypt_generic($cypher, $data);

            // clean up
            mcrypt_generic_deinit($cypher);
            mcrypt_module_close($cypher);

            return rtrim($decrypted, $padding);
    }

    return false;
}

$enc = "mkuni1LKnp7aZ6ii5oYffuuNum2MkgTLul62HYGmwESI2A7KMGR3DE4N+/38tXAELtLR+7nMqJHE2lrHIFdjow==";
$secret = "api_code";
$iv = "api_id";

$res = decrypt_data(base64_decode($enc), $secret, $iv);

if ($res == "This message was encrypted by Python, lovely, isn't it\r\n")
{ echo("SUCCESS"); }

else
{ echo("FAILED"); }

