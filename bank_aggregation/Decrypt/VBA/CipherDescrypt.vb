Imports System
Imports System.Security.Cryptography
Imports System.Text

Module VBModule
    public shared function DecryptData(data as string, key as string, iv as string) as string
        dim key_size as integer = 16
        dim iv_size as integer = 16
        dim padding as char = "$"

        key = key.PadRight(key_size, padding)
        iv = iv.PadRight(iv_size, padding)
        
        if key.Length > 16 then
            key = key.Substring(0, 16)
        end if
        
        if iv.Length > 16 then
            iv = iv.Substring(0, 16)
        end if

        dim keyArray as byte() = UTF8Encoding.UTF8.GetBytes(key)
        dim ivArray as byte() = UTF8Encoding.UTF8.GetBytes(iv)
        dim toEncryptArray as byte() = Convert.FromBase64String(data)
        dim rijndaelManaged as RijndaelManaged = new RijndaelManaged()
        rijndaelManaged.Key = keyArray
        rijndaelManaged.IV = ivArray
        rijndaelManaged.Mode = CipherMode.CBC
        rijndaelManaged.Padding = PaddingMode.Zeros
        dim cryptoTransform as ICryptoTransform = rijndaelManaged.CreateDecryptor()
        dim resultArray as byte() = cryptoTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length)
        dim resultString as string = UTF8Encoding.UTF8.GetString(resultArray)
        return resultString.Remove(resultString.Length - 8)
    end function
    
    sub Main(args as string())
        dim enc as string = "mkuni1LKnp7aZ6ii5oYffuuNum2MkgTLul62HYGmwESI2A7KMGR3DE4N+/38tXAELtLR+7nMqJHE2lrHIFdjow=="
        dim secret as string = "api_code"
        dim iv as string = "api_id"

        dim res as string = DecryptData(enc, secret, iv)
        Console.WriteLine("Decrypted Data: "+ res)
        Console.WriteLine("Original Data: "+ "This message was encrypted by Python, lovely, isn't it" & vbCrLf)
        
    end sub

end Module