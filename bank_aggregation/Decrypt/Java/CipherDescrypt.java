import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class CipherDescrypt {
	static final String RAWTEXT = "This message was encrypted by Python, lovely, isn't it\r\n";
	
	public static void main(String[] args) {
		final String encryptText = "mkuni1LKnp7aZ6ii5oYffuuNum2MkgTLul62HYGmwESI2A7KMGR3DE4N+/38tXAELtLR+7nMqJHE2lrHIFdjow==";
		
		String result = descrypt(encryptText);
		if(result.equals(RAWTEXT)) {
			System.out.println("SUCCESS");
		} else {
			System.out.println("FAILED");
		}
	}

	private static String descrypt(String encryptText) {
		final byte[] decodeText = Base64.getDecoder().decode(encryptText.getBytes());
		final char padding = '$';
		String resultData = "";
		
		String keyFormat = String.format("%-16s", "api_code").replace(' ', padding);
		String ivFormat = String.format("%-16s", "api_id").replace(' ', padding);
		
		if (keyFormat.length() > 16) {
  			keyFormat = keyFormat.substring(0, 16);
		}
		
		if (ivFormat.length() > 16) {
  			ivFormat = ivFormat.substring(0, 16);
		} 
		
		try {
			Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding");
			cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(keyFormat.getBytes(), "AES"), new IvParameterSpec(ivFormat.getBytes()));
			resultData = new String(cipher.doFinal(decodeText)).replace("$", "");
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		return resultData;
	}
}