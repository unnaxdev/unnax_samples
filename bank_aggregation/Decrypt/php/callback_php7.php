<?php
function decrypt_data($data, $key, $iv) {
$padding = '$';
$size = 16;

// make sure strings
$key = substr(str_pad($key, $size, $padding), 0, $size);
$iv = substr(str_pad($iv, $size, $padding), 0, $size);

$method = 'aes-128-cbc';
$decoded_data = base64_decode($data);
$result = openssl_decrypt(base64_decode($data), $method, $key, OPENSSL_RAW_DATA | OPENSSL_ZERO_PADDING, $iv);
return rtrim($result, "$");
}

$content = "REPLACE THIS WITH THE data element of the callback";
echo decrypt_data($content, 'replace with merchant_api_code', 'replace with merchant_api_id');  
?>