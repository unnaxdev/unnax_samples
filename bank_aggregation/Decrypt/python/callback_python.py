import base64
from Crypto.Cipher import AES

# key = "api_code"
# iv = "api_id"
# text = callback to decrypt

def decrypt_data(text, key, iv):
    padding = '$'
    key_size = 16
    key = key.ljust(key_size, padding)[:key_size]
    iv_size = 16
    iv = iv.ljust(iv_size, padding)[:iv_size]

    cipher = AES.new(key, mode=AES.MODE_CBC, IV=iv)

    dec_data = base64.b64decode(text)
    text_data = cipher.decrypt(dec_data)
    return text_data.decode().strip(padding)

original = "This message was encrypted by Python, lovely, isn't it\r\n"
enc = "mkuni1LKnp7aZ6ii5oYffuuNum2MkgTLul62HYGmwESI2A7KMGR3DE4N+/38tXAELtLR+7nMqJHE2lrHIFdjow==";
secret = "api_code";
iv = "api_id";

res = decrypt_data(enc, secret, iv)

if (res == original):
    print("SUCCESS", res)

else:
    print("FAIL")