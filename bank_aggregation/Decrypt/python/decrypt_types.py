import os
from Crypto.Cipher import AES
import base64
import json


BLOCK_SIZE = 32
IV_SIZE = 16
KEY_SIZE = 16
PADDING = '$'


class CallbackFiles:
    API_ID = "_your_api_id_"
    API_CODE = "_your_api_code_"
    STORE_PATH = "/tmp/var/..."
    class EventType:
        JSON = 'fitnance_read'
        EXCEL = 'fitnance_read_excel'
        PDF = 'fitnance_read_pdf'

    def save_data(self, json_request): #Callback content       
        
        request_code = json_request.get('response_id')        
        event_type = json_request.get('triggered_event', self.EventType.JSON)
        completed_on = json_request.get('date')  # "2019-07-02 14:47:47" UTC date,

        file_type = None
        write_mode = 'w+'        

        content = self.decrypt_data(json_request.get('data'), self.API_CODE,self.API_ID)

        if event_type == self.EventType.JSON:
            file_type = '.json'
            content = json.dumps(content, indent=4)
        elif event_type == self.EventType.EXCEL:
            file_type = '.xlsx'
            content = base64.urlsafe_b64decode(content)
            write_mode = 'wb'

        elif event_type == self.EventType.PDF:
            file_type = '.pdf'
            content = base64.urlsafe_b64decode(content)
            write_mode = 'wb'

        else:
            return {'message': 'Unable to parse result'}, 200

        try:
            path = self.STORE_PATH #Path to save the file
            self.store_file(path, request_code, content, file_type, write_mode)
        except Exception:
            return False        

        return {'message': 'Callback received succesfully.'}, 200    

    def decrypt_data(self, text, key, iv):
        key = key.ljust(KEY_SIZE, PADDING)[:KEY_SIZE]
        iv = iv.ljust(IV_SIZE, PADDING)[:IV_SIZE]

        cipher = AES.new(key, mode=AES.MODE_CBC, IV=iv)

        dec_data = base64.b64decode(text)
        text_data = cipher.decrypt(dec_data)
        return text_data.decode().strip(PADDING)

    def store_file(self, path, request_code, fitnance_msg, filetype='.json', write_mode='w+'):
        request_path = f'{path}/{request_code}'

        if not os.path.exists(request_path):
            os.makedirs(request_path)

        with open(f'{request_path}/{request_code}{filetype}', write_mode) as f:
            f.write(fitnance_msg)
