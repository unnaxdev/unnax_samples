// Created By Irakli Bakuradze
using System;
using System.Security.Cryptography;
using System.Text;

namespace WidgetDecrypt
{
    public class Program
    {
        public static string DecryptData(string data, string key, string iv)
        {
            int key_size = 16;
            int iv_size = 16;
            char padding = '$';

            key = key.PadRight(key_size, padding);
            iv = iv.PadRight(iv_size, padding);
            
            if (key.Length > 16) {
				key = key.Substring(0, 16);
			}
			
			if (iv.Length > 16) {
				iv = iv.Substring(0, 16);
			}

            byte[] keyArray = UTF8Encoding.UTF8.GetBytes(key);
            byte[] ivArray = UTF8Encoding.UTF8.GetBytes(iv);
            byte[] toEncryptArray = Convert.FromBase64String(data);
            RijndaelManaged rijndaelManaged = new RijndaelManaged
            {
                Key = keyArray,
                IV = ivArray,
                Mode = CipherMode.CBC,
                Padding = PaddingMode.Zeros
            };
            ICryptoTransform cryptoTransform = rijndaelManaged.CreateDecryptor();
            byte[] resultArray = cryptoTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
            string resultString = UTF8Encoding.UTF8.GetString(resultArray);
            return resultString.Remove(resultString.Length - 8);
        }
        public static void Main(string[] args)
        {
            string enc = "mkuni1LKnp7aZ6ii5oYffuuNum2MkgTLul62HYGmwESI2A7KMGR3DE4N+/38tXAELtLR+7nMqJHE2lrHIFdjow==";
            string secret = "api_code";
            string iv = "api_id";

            string res = DecryptData(enc, secret, iv);

            if (res == "This message was encrypted by Python, lovely, isn't it\r\n")
            { Console.WriteLine("SUCCESS"); }

            else
            { Console.WriteLine("FAILED"); }
        }
    }
}
