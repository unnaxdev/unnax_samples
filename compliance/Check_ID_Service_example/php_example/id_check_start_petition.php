<?php
/**
 * Example Start CheckId Petition. 
 */

$api_url = 'http://demo.unnax.net/api/v2/checkify/check_id';

$file1 = 'WP_000455.jpg'; //must be a valid file
$file2 = 'WP_000456.jpg'; //must be a valid file

$random = rand(1,10000);

$api_id = '1234567890';
$api_code = '0987654321';

$signature = sha1($random.$api_id);

$post = array(
     "image_front"=>"@".$file1,
     "image_back"=>"@".$file2,
     "random"=>$random,
     "merchant_id"=>$api_id,
     "merchant_signature"=>$signature
);

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $api_url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
curl_setopt($ch, CURLOPT_HEADER, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type:multipart/form-data"));
$response = curl_exec($ch);
echo curl_error($ch);
curl_close($ch);

print_r($response);
