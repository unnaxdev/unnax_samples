<?php

/**
 * Example result petition for a request_id : 23 
 *  
 */

$api_url = 'http://demo.unnax.net/portal_app/api/v2/checkify/check_id/status';

$random = rand(1,10000);

$api_id = '1234567890';
$api_code = '0987654321';

$signature = sha1($random.$api_id);

$params = array(
     "request_id"=>23, //specific request_id returned by "http://demo.unnax.net/portal_app/api/v2/checkify/check_id"
     "random"=>$random,
     "merchant_id"=>$api_id,
     "merchant_signature"=>$signature
);

echo json_encode(file_get_contents($api_url."?".http_build_query($params)));
