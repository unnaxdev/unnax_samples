<?php
/**
 * Example Cashub Widget Loader from Server Side.
 * ref: http://docs.unnax.com/v2/cashub/frame/
 */
echo
"<form style='display: none' name=\"unnax_payment_data_form\" action=\"http://demo.unnax.net/api/v2/cashub/init\" method=\"post\">
    <input type=\"hidden\" name=\"merchant_id\" value=\"demo\"> 
    <input type=\"hidden\" name=\"merchant_signature\" value=\"{merchant_signature}\"> <!-- sha1(order_code + merchant_id) -->
    <input type=\"text\" name=\"amount\" value=\"10052\"/>
    <input type=\"text\" name=\"order_code\" value=\"TST00014\"/>
    <input type=\"text\" name=\"bank_order_code\" value=\"demoBankOrd123\"/>
    <input type=\"text\" name=\"concept\" value=\"Demo Concept\"/>
    <input type=\"text\" name=\"currency\" value=\"EUR\"/>
    <input type=\"hidden\" name=\"url_ok\" value=\"http://test.com/ok.php\"/>
    <input type=\"hidden\" name=\"url_ko\" value=\"http://test.com/ko.php\"/>
    <input type=\"hidden\" name=\"lang\" value=\"en\"/>
    <input type=\"hidden\" name=\"base_color\" value=\"#0098C3\"/>
    <input type=\"hidden\" name=\"has_header\" value=\"true\"/>
    <input type=\"hidden\" name=\"has_logo\" value=\"false\"/>
    <input type=\"hidden\" name=\"url_logo\" value=\"\"/>
</form>
<script language=\"JavaScript\">
        document.unnax_payment_data_form.submit();
</script>";

